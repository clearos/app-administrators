<?php

$lang['administrators_app_description'] = 'Kun Ylläpitäjät app, voit myöntää käyttöoikeuden tiettyjen sovellusten käyttäjäryhmille järjestelmän.';
$lang['administrators_app_name'] = 'Ylläpitäjät';

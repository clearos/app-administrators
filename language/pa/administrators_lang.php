<?php

$lang['administrators_app_description'] = 'ਪਰਬੰਧਕ ਐਪਲੀਕੇਸ਼ ਨੂੰ ਨਾਲ, ਤੁਹਾਨੂੰ ਸਿਸਟਮ ਤੇ ਉਪਭੋਗੀ ਦੇ ਗਰੁੱਪ ਨੂੰ ਖਾਸ ਐਪਸ ਨੂੰ ਪਹੁੰਚ ਦੇਣ ਹੋ ਸਕਦਾ ਹੈ.';
$lang['administrators_app_name'] = 'ਪਰਬੰਧਕ';

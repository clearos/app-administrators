<?php

$lang['administrators_app_description'] = 'ನಿರ್ವಾಹಕರು ಅಪ್ಲಿಕೇಶನ್ನೊಂದಿಗೆ, ನಿಮ್ಮ ಗಣಕದ ಬಳಕೆದಾರರ ಗುಂಪುಗಳಿಗೆ ನಿರ್ದಿಷ್ಟ ಅಪ್ಲಿಕೇಶನ್ಗಳು ಪ್ರವೇಶವನ್ನು ನೀಡುತ್ತವೆ.';
$lang['administrators_app_name'] = 'ನಿರ್ವಾಹಕರು';
